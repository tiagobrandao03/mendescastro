<footer>
  <div class="footer-copy">
    <div class="footer-content d-flex flex-column align-items-center justify-content-center">
      <span class="address text-center">
        Rua dos Guajajaras 870, 9º andar- Centro, Belo Horizonte - MG, 30180-100 
      </span>
      <span class="tel text-center">
        (31) 3274-9001 (31) 99981-3151
      </span>
      <span class="social text-center">
        <ul class="text-center">
          <li><a href="https://www.facebook.com/Construtora-Mendes-e-Castro-112439280278992/?ref=bookmarks"><i class="fab fa-facebook"></i></a></li>
          <!-- <li><i class="fab fa-twitter"></i></li> -->
          <li><a href="https://www.instagram.com/construtoramendesecastro/"><i class="fab fa-instagram"></i></a></li>
        </ul>
      </span>
      <span class="copyright text-center">
        <i class="far fa-copyright"></i> 2020 Todos os Direitos Reservados
      </span>
    </div>
  </div>



  <!-- Whatsapp Call -->
  <div class="whatsapp">
    <a href="https://wa.me/5531999813151/?text=Olá%20estou%20no%20seu%20site%20e%20gostei%20dos%20imóveis,%20você%20pode%20me%20ajudar?" target="_blank"><img src="img/whatsapp.png" alt="" width="70px"></a>
  </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<script>
  $(document).ready(function(){
    clear()
    if(window.location.href.indexOf("index") != -1){
      $(".home").addClass("link-active");
    }else if(window.location.href.indexOf("apartamentosavulsos") != -1){
      $(".apartamentosavulsos").addClass("link-active")
    }else if(window.location.href.indexOf("prontosparamorar") != -1){
      $(".prontosparamorar").addClass("link-active")
    }else if(window.location.href.indexOf("lancamentos") != -1){
      $(".lancamentos").addClass("link-active")
    }else if(window.location.href.indexOf("contato") != -1){
      $(".contato").addClass("link-active")
    }
  })

  function clear(){
    $(".home").removeClass("link-active");
    $(".apartamentosavulsos").removeClass("link-active");
  }
</script>
