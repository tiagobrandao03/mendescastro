<?php 
  include("includes/header.php");
?>
<!-- Começo vilaggio -->



<div class="container-fluid">
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
        <img src="img\vilagio_palmeiras\ALAS.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text">Ed. Villaggio Palmeiras - 2 qtos / 1 suite / 2 vagas -57m2</p>
        </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
        <img src="img\solar\slide4.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text">Ed. Solar dos Montes - 3 qtos / 1 suite / 2 vagas - 100m2</p>
        </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>
<div class="row">
<div class="col-md-2"></div>
    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
        <img src="img\sidonio\SALA.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text">Ed. Sidônio Maias - 2 qtos / 1 suite / 2 vagas -90m2</p>
        </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
        <img src="img\parcaquarius\slide2.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text">Ed. Parc Aquarius - 3 qtos / 1 suite / 2 vagas - 77 e 84m2</p>
        </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
        <img src="img\sidonio\SALA.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text">Ed. Sidônio Maias - 2 qtos / 1 suite / 2 vagas -90m2</p>
        </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
        <img src="img\parcaquarius\slide2.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text">Ed. Parc Aquarius - 3 qtos / 1 suite / 2 vagas - 77 e 84m2</p>
        </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
        <img src="img\manhattan\slide2.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text">Ed. Manhattan - 3 qtos / 1 suite / 2 vagas - 92m2</p>
        </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
        <img src="img\jardim\slide2.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text">Ed. Jardins - 3 qtos / 1 suite / 2 vagas - 96m2</p>
        </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>
</div>




    <!-- Fim apartamentos avulsos -->
    <?php
    include("includes/footer.php");
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/all.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script> -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js" type="module"></script>

    <script>
    var swiper = new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
        type: 'progressbar',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>

  <script>
    


    // MENU
    $("nav ul li a").click(function(){
      $("nav ul li a").removeClass("link-active");
      $(this).addClass("link-active");
    })

    $(document).scroll(function(e){
      let posicao = $(this).scrollTop();

        if(posicao > 122){
          $(".welcome").fadeIn(1000);
          $(".navbar-site").addClass("nav-fixed");
        }else{
          $(".navbar-site").removeClass("nav-fixed");
        }

        if(posicao > 800){
          $(".services").fadeIn(1000)
        }

        if(posicao > 1600){
          $(".rent").fadeIn(1000);
        }

        if(posicao > 2500){
          $(".locale").fadeIn(1000);
        }
      
        if(posicao > 2800){
          $(".numeros").show();
        }
      
    })

    $(document).ready(function(){
      $('.counter').counterUp({
        delay: 10,
        time: 1000
      });
    })

  </script>
</body>
</html>