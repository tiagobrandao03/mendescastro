<?php 
  include("includes/header.php");
?>
<!-- Começo apartamentos avulsos -->

<!-- Começo vilaggio -->

<!-- imagem carousel -->



  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <!--  -->
                <div class="row">
                    <div class="col-md-4">
                        <img src="img/villa_santorini/slide1.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="col-md-4">
                        <img src="img/villa_santorini/slide2.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="col-md-4">
                        <img src="img/villa_santorini/slide3.jpg" class="d-block w-100" alt="...">
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                 <!--  -->
                 <div class="row">
                    
                    <div class="col-md-4">
                        <img src="img/villa_santorini/slide4.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="col-md-4">
                        <img src="img/villa_santorini/slide5.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="col-md-4">
                        <img src="img/villa_santorini/slide6.jpg" class="d-block w-100" alt="...">
                    </div>
                </div>
            </div>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>





    <!-- fim imagem carousel -->

  <div class="col-md-12 row" style="margin-top:40px;">
    <div class="col-md-2"></div>    
    <!--começo texto -->
    <div class="col-md-4" style="padding-right:40px;">
      <div class="text-center" style="padding-top:20px;padding-bottom:20px;">
        <h2 class="" style="padding-bottom:20px;">Villa Santorini</h2>
        <img class="text-center" src="img/divider_line_big.svg">
      </div>

      <h6 style="color: #475764;">Características:</h6>
      <ul class="rob-of">
        <li style=" margin-top:10px;">68 m²</li> 
        <li>3 qts com 1 Suíte</li>
        <li>2 Vagas</li>
        <!-- <li>Salão de Festas / Espaco Gourmet / Salão de Jogos</li>     
        <li>Kids / Academia</li>        -->
        <li>Porcelanato / Granito</li>  
        <li>Elevador</li>
        <!-- <li class="padding-bottom:20px;"> Água e Gás Individuais</li> -->
      </ul>

      <h6 style="color: #475764;">Atendimento:</h6>
      <p>Clique agora em nosso link do whastapp e fale com um de nossos especialistas.</p>
      <p  style="color: #475764;"></p>
    </div>
    <!--fim texto  -->
    <!-- form -->
    <div class="col-md-4" style="margin-left:10px; margin-bottom:50px; margin-top:1px;">
        <div class="col-md-10 " style="margin-left: 20px;margin-top:20px;margin-bottom:20px; padding-left:40px;">
        <form class="">
        <!-- H2 formulario -->
        <div class="text-center" style="padding-bottom:20px;">
            <h2 class="" style="padding-bottom:20px;">Dúvidas ?</h2>
            <img class="text-center" src="img/divider_line_big.svg">
        </div>
        <!-- Text area -->
        <div class="form-group" style="padding-bottom:20px;">
            <label for="exampleFormControlTextarea1" style="font-weight:bold; color: #475764; font-size:12px;">Escreva aqui</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        <!-- nome -->
        <div class="form-group" style="padding-bottom:20px;">
            <label for="exampleInputEmail1" style="font-weight:bold; color: #475764; font-size:12px;">Nome (obrigatório)</label>
            <input type="name" class="form-control" id="exampleInputName" aria-describedby="emailHelp">
            <small id="nameHelp" class="form-text text-muted" style="font-size:12px;" >Digite o seu nome aqui</small>
        </div>
        <!-- email -->
        <div class="form-group" style="padding-bottom:20px;">
            <label for="exampleInputEmail1" style="font-weight:bold; color: #475764; font-size:12px;">Email (obrigatório)</label>
            <input type="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp">
            <small id="emailHelp" class="form-text text-muted" style="font-size:12px;" >Digite o seu nome aqui</small>
        </div>
         <!-- telefone -->
         <div class="form-group" style="padding-bottom:20px;">
            <label for="exampleInputEmail1" style="font-weight:bold; color: #475764; font-size:12px;">Telefone (obrigatório)</label>
            <input type="phone" class="form-control" id="phoneInputEmail" aria-describedby="phoneHelp">
            <small id="phoneHelp" class="form-text text-muted" style="font-size:12px;" >Digite o seu telefone aqui</small>
        
            <button type="submit" class="btn btn-primary" style="font-weight:bold; border-radius:22px;">Clique aqui</button>
            </form>

        </div>
    </div>
    <!-- fim form -->
    </div>
    <!-- fim conteudo  -->


    <!-- ALBUM -->
    <!-- <section class="p-0 pt-5">
      <div class="container-fluid">
     
        <div class="col-md-12 text-center rent" style=" padding-right:60px;"> 
          <h2>Conheça Nossos Empreendimentos</h2>
          <img src="img/divider_line_big.svg">
          <div class="paragraph d-flex flex-row justify-content-center">
            <p>
           
            </p>
          </div>
         
          <div class="col-sm-12 col-md-12">
     
            <div class="d-flex flex-row flex-wrap justify-content-center">
              <div class="col-sm-12 col-md-8 pl-5 pr-0">
                <div style="width: 100%;height:100%;">
               
                  <img class="img-fluid" src="img/vilagio_palmeiras/planta1.jpg" style="width:100%;height:480px;max-height:475px;">
                </div>
              </div> 
              <div class="col-sm-12 col-md-4 pl-1 pr-0"> 
                <div class="col-sm-12 col-md-12 pl-0">
                  <div style="width: 100%;height:100%;">
                
                    <img class="img-fluid" src="img/vilagio_palmeiras/lazer.jpg" alt="" style="width:100%;height:235px;">
                  </div>
                </div>
                <div style="margin-top:4px; "></div>
             
                <div class="col-md-12 pl-0">
                  <div style="width: 100%;height:100%;">
                    <img class="img-fluid" src="img/vilagio_palmeiras/ALAS2.jpg" alt="" style="width:100%;height:235px;">
                  </div>
                  
                </div>
                
              </div>
              
            </div> 
          </div>

          <div class="col-sm-12 col-md-12 mt-1">
         
            <div class="d-flex flex-row flex-wrap justify-content-center">
              <div class="col-sm-12 col-lg-4 pl-5 pr-0"><img class="img-fluid" src="img/vilagio_palmeiras/planta2.jpg" alt="" style="width: 100%; height:240px;" ></div>
              <div class="col-sm-12 col-lg-4 pr-0 pl-1"><img class="img-fluid" src="img/vilagio_palmeiras/planta3.jpg" alt="" style="width: 100%; height:240px;"></div>
              <div class="col-sm-12 col-lg-4 pl-1"><img class="img-fluid" src="img/vilagio_palmeiras/garagem.jpg" alt="" style="width: 100%; height:240px;" ></div>
            </div>
        
          </div>
        </div>
      </div>
    </section> -->
    <!-- FIM ALBUM -->
</div>
<!-- fim vilaggio -->

    <!-- Fim apartamentos avulsos -->
    <?php
    include("includes/footer.php");
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/all.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script> -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js" type="module"></script>

    <script>
    var swiper = new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
        type: 'progressbar',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>

  <script>
    


    // MENU
    $("nav ul li a").click(function(){
      $("nav ul li a").removeClass("link-active");
      $(this).addClass("link-active");
    })

    $(document).scroll(function(e){
      let posicao = $(this).scrollTop();

        if(posicao > 122){
          $(".welcome").fadeIn(1000);
          $(".navbar-site").addClass("nav-fixed");
        }else{
          $(".navbar-site").removeClass("nav-fixed");
        }

        if(posicao > 800){
          $(".services").fadeIn(1000)
        }

        if(posicao > 1600){
          $(".rent").fadeIn(1000);
        }

        if(posicao > 2500){
          $(".locale").fadeIn(1000);
        }
      
        if(posicao > 2800){
          $(".numeros").show();
        }
      
    })

    $(document).ready(function(){
      $('.counter').counterUp({
        delay: 10,
        time: 1000
      });
    })

  </script>
</body>
</html>