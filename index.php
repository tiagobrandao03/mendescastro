<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://fonts.googleapis.com/css?family=Oswald|Roboto&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
  
  <link rel="stylesheet" href="css/count3_estilo.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/all.min.css">
  <link rel="stylesheet" href="css/swiper.min.css">
  
  <title>Mendes e Castro Construtora</title>

  <style>
    /* font-family: 'Roboto', sans-serif;
font-family: 'Oswald', sans-serif; */
    .swiper-container {
      width: 100%;
      height: 100%;
    }

    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }

    .swiper-pagination-progressbar 
    .swiper-pagination-progressbar-fill {
      background: rgba(144,28,64,0.7);
    }

    .swiper-button-prev .swiper-button-next {
      color:#fff;
    }

    :root{--swiper-theme-color:rgb(144,28,64)}

  </style>

</head>
<body>
  <?php include("includes/header.php"); ?>

  <main>
    <!-- Swiper -->
    <!-- <div class="swiper-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide" style="border:3px solid red;">
          <video id="videoplay" src="videos/vídeo_campanha_villa_santorini.mp4" type="video/mp4" autoplay loop muted width="100%"></video>
        </div>
        <div class="swiper-slide">
          <img src="img/9a6f86_ff80bc61a20540218cbedcacd10946dc.png" alt="">
        </div>
        <div class="swiper-slide">Slide 3</div>
        <div class="swiper-slide">Slide 4</div>
        <div class="swiper-slide">Slide 5</div>
        <div class="swiper-slide">Slide 6</div>
        <div class="swiper-slide">Slide 7</div>
        <div class="swiper-slide">Slide 8</div>
        <div class="swiper-slide">Slide 9</div>
        <div class="swiper-slide">Slide 10</div>
      </div>
      Add Pagination -->
      <!-- <div class="swiper-pagination"></div> -->
      <!-- Add Arrows -->
      <!-- <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div> -->
    <!-- </div> -->
    <div class="container-fluid mt-4" style="height:80vh; max-height:80vh">
      <div class="row">
        <div class="col-md-12" style="max-height:80vh;min-height:80vh;">
          <video id="videoplay" src="videos/vídeo_campanha_villa_santorini.mp4" type="video/mp4" autoplay loop muted style="width:100%;max-height:100%;height:100%"></video>
        </div>
      </div>
    </div>

    <div class="container-fluid " style="position:relative;">
    <section class="p-0 pt-5 pb-5 traco">
      <div class="container-fluid">
        <!-- SERVIÇOS background: #f2f2f2-->
        <div class="box-spacing col-md-12 pt-5 pb-5 text-center services" style=" ">
          <h2>Conheça Nossos Produtos</h2>
          <img src="img/divider_line_big.svg" class="attachment-full size-full" alt="" data-attachment-id="47" data-permalink="https://themes.getmotopress.com/aquentro/home/divider_line_big/" data-orig-file="https://themes.getmotopress.com/aquentro/wp-content/uploads/sites/20/2018/07/divider_line_big.svg" data-orig-size="" data-comments-opened="1" data-image-meta="[]" data-image-title="divider_line_big" data-image-description="" data-medium-file="https://themes.getmotopress.com/aquentro/wp-content/uploads/sites/20/2018/07/divider_line_big.svg" data-large-file="https://themes.getmotopress.com/aquentro/wp-content/uploads/sites/20/2018/07/divider_line_big.svg">
          <div class="paragraph d-flex flex-row justify-content-center">
            <p>
              
            </p>
          </div>
         

          <div class="slider-secondary  pt-1">
            <div class="col-md-12  slide-box-width">
              <div class="slider_style col-md-5 col-sm-12 pl-5">
                <div class="row row-style">
                <div class="col-md-6 col-sm-12 slide-counter">
                  <button class="btn btn-light first-counter" onclick="prev()"><</button>
                  <button class="btn btn-light first-counter" onclick="next()">></button></div>
                </div>
                <div class="slide-details ">
                  <h2 class="painel-titulo">Villaggio Palmeiras</h2>
                  <ul class="lista rob-of" style="padding:0px 0px 0px 20px;margin:0px 0px 6px 0px;">
                    <li class="painel-area" style=" margin-top:10px;">. 57 m²</li> 
                    <li class="painel-quartos">. 2 Qtos , 1 Suíte , 2 Vagas</li>
                    <li class="painel-outros">. Salão de Festas , Espaco Gourmet , Salão de Jogos</li>     
                    <li class="painel-academia-kids">. Kids , Academia</li>       
                    <li class="painel-material">. Porcelanato , Granito</li>  
                    <li class="painel-elevador">. Elevador</li>
                    <li class="painel-agua-gas">. Água e Gás Individuais</li>
                  </ul>
                  <a href="ed_vilaggio_palmeiras.php" class="more-link detalhes-link" tabindex="0">Detalhes</a>
                  <i class="fas fa-caret-right"></i>
                </div>
              </div>
              <div class="slider_style2 d-flex img-box-flex-tipo1 col-md-7 col-sm-12 pr-0  justify-content-center ">
                <!-- <img src="http://via.placeholder.com/709x430" alt="slide-photo">
                 -->
                <img class="slider_style2 atual" src="img/9a6f86_bb03e751f4604d0a9579a45a547e19e6.png" alt="" width="709" height="430">
              </div>
              
            </div>
            </div>
        </div>
      </div>
    </section>
    </div>

    <div class="traco" style="background-color:#f2f2f2;position:relative;">
      <div class="container-fluid" style="padding:140px 0px 130px 0px;">
      <!-- BEM VINDO background-image:url('img/86373.jpg')-->
      <div class="col-md-12 col-sm-12 d-flex flex-row welcome" style=" padding:10px 90px 10px 90px;">
        <div class="box_style col-md-6 col-sm-12" style="margin-bottom:10px; margin-top:10px;">
          <aside class="">
            <!-- countBox -->
            <h2>More no Melhor da Região Oeste</h2>
            <img src="img/divider_line_big.svg">
          
            <p class="p-style" style=" padding:0px 70px 0px 0px;">
              Morar neste apartamento do bairro Palmeiras é estar certo de que você irá viver momentos inesquecíveis.
              Este imóvel lhe proporciona o ambiente perfeito para se divetir em família e receber amigos. São 3 quartos com suíte 1 ou 2 vagas.
              Situado em um dos melhores pontos da região oeste de Belo Horizonte, o edifício possui uma estrutura que entrega a você uma
              experiência de bem-estar.
            </p>

            <div class="countBox data-site d-flex flex-row align-items-center justify-content-center" style="margin-top:50px;">
              <div class="col-md-12 container counter-selection">
              <div class="d-flex flex-row text-center" style="margin-left:20px; margin-right:20px;">
              <div class="row">
                  <div class="col-md-3 d-flex flex-column text-center col-md-3 counter-box">
                    <span class="number ">67</span>
                    m²
                  </div>
                  <div class="col-md-3 d-flex flex-column text-center counter-box">
                    <span class="number">3</span>
                    Quartos
                  </div>
                  <div class="col-md-3 d-flex flex-column text-center counter-box">
                    <span class="number ">1</span>
                    Suíte
                  </div>
                  <div class="col-md-3 d-flex flex-column text-center counter-box">
                    <span class="number ">2</span>
                    Vagas
                  </div>
                  </div>
              </div>
              </div>
            </div>
          </aside>
        </div>
        <div class="count-box-format box_style2 col-md-6 col-sm-12 d-flex flex-row justify-content-center ed_img_style" style="padding-top:0px;">
          <img class="img-style" src="img/imagem_site_villa_santorini_002_ab .png" width="auto" height="600"  >
        </div>
      </div>
    </div>
    </div>
    
    <section class="p-0 pt-5">
      <div class="container-fluid">
        <!-- ALBÚM -->
        <div class="col-md-12 text-center rent" style=" padding-right:60px;"> 
          <h2 class="h2-style">Conheça Nossos Empreendimentos</h2>
          <img src="img/divider_line_big.svg">
          <div class="paragraph d-flex flex-row justify-content-center">
          </div>
          <!-- fim col-md-12 -->
          <div class="col-sm-12 col-md-12">
            <!-- fim col-md0 -->
            <div class="d-flex flex-row flex-wrap justify-content-center">
              <div class="col-sm-12 col-md-8 pl-5 pr-0">
                <div style="width: 100%;height:100%;">
                  <!-- <img class="img-fluid" src="http://via.placeholder.com/1000x600" alt=""> -->
                  <a href="ed_solar.php">
                  <img class="img-fluid img-galeria-define" src="img/solar/slide1.jpg" style="width:100%;height:480px;max-height:475px;"></a>
                </div>
              </div> 
              <div class="col-sm-12 col-md-4 pl-1 pr-0"> 
                <div class="col-sm-12 col-md-12 pl-0">
                  <div style="width: 100%;height:100%;">
                    <!-- <img class="img-fluid" src="http://via.placeholder.com/400x330" alt="" margin-right:105px; height:200px; ;> -->
                    <a href="ed_villa_santorini.php">
                    <img class="img-fluid image-galeria-define2" src="img\villa_santorini\slide1.jpg" alt="" style="width:100%;height:235px;"></a>
                  </div>
                </div>
                <div style="margin-top:4px; "></div>
                <!-- <div class="col-md-12 col_style_3 back-image-2"> -->
                <div class="col-md-12 col-sm-12 pl-0 ">
                  <div style="width: 100%;height:100%;">
                  <a href="ed_mirante_sol.php">
                    <img class="img-fluid image-galeria-define3" src="img\mirante\slide1.jpg" alt="" style="width:100%;height:235px;"></a>
                  </div>
                    <!-- <img class="img-fluid" src="http://via.placeholder.com/400x330" alt="" margin-right:105px; height:200px; ;> -->
                    <!-- <img class="img-fluid" src="http://via.placeholder.com/400x330" alt="" margin-right:105px; height:255px; padding-left:0px;> -->
                    <!-- <img class="image_style_3" src="img/predio.png"> -->
                </div>
                
              </div>
              <!-- fim d-flex -->
            </div> 
          </div>

          <div class="col-sm-12 col-md-12 mt-1">
            <!-- row -->
            <div class="d-flex flex-row flex-wrap justify-content-center">
              <div class="col-sm-12 col-lg-4 pl-5 pr-0 img_manhata">
              <a href="ed_manhattan.php">
              <img class="img-fluid image-galeria-define4" src="img\manhattan\slide1.jpg" alt="" style="width: 100%; height:240px;" >
              </a></div>
              <div class="col-sm-12 col-lg-4 pr-0 pl-1 ">
              <a href="ed_jardim.php">
              <img class="img-fluid image-galeria-define5" src="img\jardim\GARAGEM.jpg" alt="" style="width: 100%; height:240px;"></a>
              </div>
              <div class="col-sm-12 col-lg-4 pl-1 ">
              <a href="ed_parc_aquarius.php">
              <img class="img-fluid image-galeria-define6 " src="img\parcaquarius\slidefachada.jpg" alt="" style="width: 100%; height:240px;" >
              </a>
              </div>
            </div>
          <!-- fim col-md-12 -->
          </div>
        </div>
      </div>
    </section>

    
    <div class="numeros traco mt-5" style="background-color:#f2f2f2;position:relative;">
        <div class="container-fluid" style="padding:40px 0px 10px 0px;">
            <!-- BEM VINDO background-image:url('img/86373.jpg')-->
            <div class="col-md-12 col-sm-12 d-flex flex-row welcome" style=" padding:10px 10px 0px 50px;">
                <div class="col-md-12 col-sm-12" style="margin-bottom:50px; margin-top:10px;">
                    <aside>
                        <div class="h2-style2 text-center">
                            <h2 class="">Nossos Números</h2>
                            <img src="img/divider_line_big.svg">
                        </div>

                        <p style="padding:0px 0px 0px 0px;">
                            <!-- texto -->
                        </p>

                        <div class="costyle data-tipob d-flex flex-row align-items-center justify-content-center"
                            style="margin-top:10px;">
                            <div class="col-md-12 container counter-selection">

                                <div class="span_style d-flex flex-row text-center"
                                    style="margin-left:1px; margin-right:1px;">
                                    <di class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="row">
                                                <div class="col-md-4 d-flex flex-column text-center counter-box ">
                                                    <span class=" counter countClass">12</span>
                                                    <p class="pClass_style_count"> Anos de experiência</p>
                                                </div>
                                                <div class="col-md-4 d-flex flex-column text-center counter-box">
                                                    <div
                                                        class="d-flex flex-row align-items-center justify-content-center">
                                                        <span style="font-size:50px;">+</span>
                                                        <span class=" counter countClass">400</span>
                                                    </div>
                                                    <p class="pClass_style_count">Clientes Satisfeitos</p>
                                                </div>
                                                <div
                                                class="col-md-4  d-flex flex-row justify-content-center align-items-center">
                                                <img class="countClass" src="img/selo.png" alt="" width="150px">
                                            </div>
                                            </div>
                                            
                                        </div>
                                        <!-- count-box-format2
-->
                                        <div class="count-box-formt2 col-md-5 col-sm-12 ">
                                            <div class=" row ">
                                                <div class=" d-flex flex-column "
                                                    style="margin-top:40px;margin-right:150px;">
                                                    <div class=" col-md-12 ">
                                                        <div class="depoimento_1">
                                                            <!--  -->
                                                            <div class="testimonial-father" style="width: 100%;">
                                                                <!--  -->
                                                                <!-- <div class="photos"> -->
                                                                <img style="width:100px;height:100px;border-radius:100%;"
                                                                    src="img/dep-2.png" alt="">
                                                            </div>
                                                            <div class="testimonial-entry-content">
                                                                <p>“Fui muito feliz na aquisição de um imóvel no Ed.
                                                                    Villaggio Palmeiras. A construtora tratou todo o
                                                                    processo de forma íntegra e me forneceu as
                                                                    informações necessárias para conclusão do negócio
                                                                    com eficiência e segurança. Amo meu apartamento e
                                                                    super indico a empresa.”</p>
                                                                <span class="testimonial-entry-title">― Priscilla
                                                                    Mansur</span>
                                                            </div>
                                                        </div>
                                                        <div class="depoimento_2" style="display: none;">
                                                            <!--  -->
                                                            <div class="testimonial-father" style="width: 100%;">
                                                                <!--  -->
                                                                <!-- <div class="photos"> -->
                                                                <img style="width:100px;height:100px;border-radius:100%;"
                                                                    src="img/dep-1.png" alt="">
                                                            </div>
                                                            <div class="testimonial-entry-content">
                                                                <p>“Quando conheci o projeto da Construtora Mendes e
                                                                    Castro (Ed. Villaggio Palmeiras), fui muito bem
                                                                    atendida pela Danúbia e posteriormente por demais
                                                                    membros da equipe. Durante toda a obra, recebi as
                                                                    informações necessárias em tempo hábil. Tanto a
                                                                    venda quanto o pós-venda ocorreram de forma
                                                                    tranquila. Hoje eu e minha família estamos
                                                                    satisfeitos com nossa aquisição e sempre indicamos a
                                                                    empresa.”</p>
                                                                <span class="testimonial-entry-title">― Luciana
                                                                    Rocha</span>
                                                            </div>
                                                        </div>
                                                        <div class="depoimento_3" style="display: none;">
                                                            <!--  -->
                                                            <div class="testimonial-father" style="width: 100%;">
                                                                <!--  -->
                                                                <!-- <div class="photos"> -->
                                                                <img style="width:100px;height:100px;border-radius:100%;"
                                                                    src="img/dep-3.png" alt="">
                                                            </div>
                                                            <div class="testimonial-entry-content">
                                                                <p>“Só tenho coisas boas para falar sobre a Construtora
                                                                    Mendes e Castro, na pessoa do Sr. Breno e da
                                                                    Danúbia, corretora que me atendeu. Eles me ajudaram
                                                                    de inúmeras formas a realizar o sonho de ter meu
                                                                    apartamento quando eu e minha esposa achávamos ser
                                                                    impossível. Agradeço à Deus todos os dias pela vida
                                                                    dessa empresa e de toda equipe.”</p>
                                                                <span class="testimonial-entry-title">― Ilson Teixeira e
                                                                    família</span>
                                                            </div>
                                                        </div>
                                                        <!-- aqui -->
                                                        <div class="col-md-12">
                                                            <div class=" d-flex flex-row justify-content-center">
                                                                <ul class="slick-dots" style="display: flex;"
                                                                    role="tablist">
                                                                    <li onclick="showComment(1)"
                                                                        class="slick-active active comment1 comment"
                                                                        role="presentation">
                                                                        <button type="button" role="tab"
                                                                            id="slick-slide-control10"
                                                                            aria-controls="slick-slide10"
                                                                            aria-label="1 of 3" tabindex="0"
                                                                            aria-selected="true">1</button>
                                                                    </li>
                                                                    <li onclick="showComment(2)" role="presentation"
                                                                        class="comment2 comment">
                                                                        <button type="button" role="tab"
                                                                            id="slick-slide-control11"
                                                                            aria-controls="slick-slide11"
                                                                            aria-label="2 of 3" tabindex="-1">2</button>
                                                                    </li>
                                                                    <li onclick="showComment(3)" role="presentation"
                                                                        class="comment3 comment">
                                                                        <button type="button" role="tab"
                                                                            id="slick-slide-control12"
                                                                            aria-controls="slick-slide12"
                                                                            aria-label="3 of 3" tabindex="-1">3</button>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!--  -->
                                                    </div>
                                                    <!--  -->
                                                </div>
                                                <!--  -->
                                            </div>
                                        </div>
                                </div>
                                </di>

                                <!--  -->
                            </div>


                        </div>
                </div>

                </aside>
            </div>

            <!-- <div class="col-md-6 col-sm-12 d-flex flex-row justify-content-center ed_img_style" style="padding-top:0px;">
  
    </div> -->
        </div>
  </div>
</div>
    

    <section class="neigborhood p-0 pt-5 pb-5">
      <div class="container-fluid">
        <!-- LOCAIS -->
        <div class="col-md-12 pt-5 pb-5 text-center locale" style="">
        <!-- background: #f2f2f2 -->
          <h2>Notícias Recentes</h2>
          <img src="img/divider_line_big.svg">

          <div class="col-lg-12 col-md-12 d-flex flex-row pt-5">
          <div class="row">
            <div class="col-lg-3 col-md-12 col-sm-12 text-left pl-4">
              <div class="card-block">
                <div class="number">
                  01.
                  <span class="square"></span>
                </div>
                <h2 class="block-title">
                  <a class="square-link" href="#">Conheça a região Oeste</a>
                </h2>
                <img class="square-img" src="img/divider_line_big.svg">
                <p>
                  A região oeste  é uma região administrativa no município brasileiro de Belo Horizonte,
                  no estado de Minas Gerais, através desta região a Linha 1 do metrô, que a atravessa,
                  segue até às Estações Cidade Industrial e Eldorado na cidade vizinha...
                </p>
              </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12 text-left pl-4">
              <div class="card-block">
                <div class="number">
                  02.
                  <span class="square"></span>
                </div>
                <h2 class="block-title">
                  <a class="square-link" href="#">O que é ITBI ?</a>
                </h2>
                <img class="square-img" src="img/divider_line_big.svg">
                <p>
                  O Imposto sobre Transmissão de Bens Imóveis – ITBI – é um tributo
                  municipal a ser pago quando ocorre uma transferência imobiliária. Por ser
                  um imposto municipal, sua cobrança é realizada pela prefeitura da cidade
                  onde fica o imóvel adquirido. Em muitas...
                </p>
              </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12 text-left pl-4">
              <div class="card-block">
                <div class="number">
                  03.
                  <span class="square"></span>
                </div>
                <h2 class="block-title">
                  <a class="square-link" href="#">Pronto ou na planta ?</a>
                </h2>
                <img class="square-img" src="img/divider_line_big.svg">
                <p style="text-align:left;">
                ​  Todo mundo procura um lugar para chamar de seu e, finalmente, parar de pagar
                  aluguel. Mas, antes de começar esse projeto, é preciso escolher a forma de
                  pagamento e o tipo de imóvel mais adequado ao seu estilo de vida: na planta ou
                  pronto para morar.
                  Veja abaixo os principais cuidados...
                </p>
              </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12 text-left pl-4">
              <div class="card-block">
                <div class="number">
                  04.
                  <span class="square"></span>
                </div>
                <h2 class="block-title">
                  <a class="square-link" href="#">Queda do Selic para 4,5%</a>
                </h2>
                <img class="square-img" src="img/divider_line_big.svg">
                <p>
                  O Comitê de Política Monetária (Copom) do Banco Central (BC) decidiu, reduzir a taxa básica de juros (Selic) de 5% para 4,5% ao ano. O corte de meio ponto percentual veio em linha com a expectativa do mercado. Com a decisão, a Selic chegou ao menor percentual desde...
                </p>
              </div>
            </div>
            </div>
          </div>

        </div>
      </div>
    </section>

  </main>

  <?php
    include("includes/footer.php");
  ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/all.min.js"></script>
  <script src="js/swiper.min.js"></script>
  <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
  <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script> -->
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
  <script src="js/jquery.counterup.min.js" type="module"></script>

  <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
        type: 'progressbar',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>

  <script>
    


    // MENU
    $("nav ul li a").click(function(){
      $("nav ul li a").removeClass("link-active");
      $(this).addClass("link-active");
    })

    $(document).scroll(function(e){
      let posicao = $(this).scrollTop();

        if(posicao > 122){
          $(".welcome").fadeIn(1000);
          $(".navbar-site").addClass("nav-fixed");
        }else{
          $(".navbar-site").removeClass("nav-fixed");
        }

        if(posicao > 800){
          $(".services").fadeIn(1000)
        }

        if(posicao > 1600){
          $(".rent").fadeIn(1000);
        }

        if(posicao > 2500){
          $(".locale").fadeIn(1000);
        }
      
        if(posicao > 2800){
          $(".numeros").show();
        }
      
    })

    $(document).ready(function(){
      $('.counter').counterUp({
        delay: 10,
        time: 1000
      });
    })

    // Comments
    function showComment(comment){
      $(".comment").removeClass("active");
      if(comment == 1){
        $(".comment1").addClass("active");
        $(".depoimento_2, .depoimento_3").hide();
        $(".depoimento_1").show();
      }else if(comment == 2){
        $(".comment2").addClass("active");
        $(".depoimento_1, .depoimento_3").hide();
        $(".depoimento_2").show();
      }else{
        $(".comment3").addClass("active");
        $(".depoimento_1, .depoimento_2").hide();
        $(".depoimento_3").show();
      }
    }

    // Imagem do slide atual
    var img = 1;

    function prev(){
      if(img>1){
        img--;
        let foto = atual();
        $(".atual").attr("src",foto);
      }
    }

    function next(){
      if(img<6){
        img++;
        let foto = atual();
        $(".atual").attr("src",foto);
      }
    }

    function atual(){

      let titulo  = $(".painel-titulo");
      let area    = $(".painel-area");
      let quartos = $(".painel-quartos");
      let outros  = $(".painel-outros");
      let academia = $(".painel-academia-kids");
      let material = $(".painel-material");
      let elevador = $(".painel-elevador");
      let aguagas  = $(".painel-agua-gas");
      let detalhes = $(".detalhes-link");

      if(img == 1){
        titulo.html("Villaggio Palmeiras");
        area.html(". 57 m²");
        quartos.html(". 2 Qtos , 1 Suíte , 2 Vagas");
        outros.html(". Salão de Festas , Espaco Gourmet , Salão de Jogos");
        academia.html(". Kids , Academia");
        material.html(". Porcelanato , Granito");
        elevador.html(". Elevador");
        aguagas.html(". Água e Gás Individuais");
        detalhes.attr("href","ed_vilaggio_palmeiras.php");
        return 'img/9a6f86_bb03e751f4604d0a9579a45a547e19e6.png';
      }else if(img == 2){
        titulo.html("Solar dos Montes");
        area.html(". 100 m²");
        quartos.html(". 3 Qtos , 1 Suíte , 2 Vagas");
        outros.html(". Lazer Completo");
        academia.html(". Elevador");
        material.html(". Água e Gás Individuais");
        elevador.html("");
        aguagas.html("");
        detalhes.attr("href","ed_solar.php");
        return 'img/solar/slide4.jpg';
      }else if(img == 3){
        titulo.html("Sidônio Maia");
        area.html(". 90 m²");
        quartos.html(". 2 Qtos , 1 Suíte , 2 Vagas");
        outros.html(". Salão de Festas , Espaço Gourmet");
        academia.html(". Academia , Saúna , SPA");
        material.html(". Elevador");
        elevador.html(". Água e Gás Individuais");
        aguagas.html("");
        detalhes.attr("href","ed_sidonio.php");
        return 'img/sidonio/slide1.jpg';
      }else if(img == 4){
        titulo.html("Park Aquarius");
        area.html(". 77 e 84 m²");
        quartos.html(". 3 qtos , 1 Suíte , 2 vagas");
        outros.html(". Salão de Festas");
        academia.html(". Espaço Gourmet");
        material.html(". Elevador");
        elevador.html(". Água e Gás Individuais");
        aguagas.html("");
        detalhes.attr("href","ed_parc_aquarius.php");
        return 'img/parcaquarius/slide2.jpg';
      }else if(img == 5){
        titulo.html("Manhattan");
        area.html(". 92 m²");
        quartos.html(". 3 Qtos , 1 Suíte , 2 Vagas");
        outros.html(". Lazer");
        academia.html(". Elevador");
        material.html(". Água e Gás Individuais");
        elevador.html("");
        aguagas.html("");
        detalhes.attr("href","ed_manhattan.php");
        return 'img/manhattan/slide5.jpg';
      }else if(img == 6){
        titulo.html("Jardins");
        area.html(". 96 m²");
        quartos.html(". 3 Qtos , 1 Suíte , 2 Vagas");
        outros.html(". Espaço Gourmet");
        academia.html(". Elevador");
        material.html(". Água e Gás Individuais");
        elevador.html("");
        aguagas.html("");
        detalhes.attr("href","ed_jardim.php");
        return 'img/jardim/slide5.jpg';
      }
    }

  </script>
</body>
</html>