<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Mendes e Castro Construtora</title>

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/all.min.css">
  <link rel="stylesheet" href="css/swiper.min.css">
  <link rel="stylesheet" href="css/style.css">

  <style>

    .swiper-container {
      width: 100%;
      height: 100%;
    }

    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }

    .swiper-pagination-progressbar 
    .swiper-pagination-progressbar-fill {
      background: rgba(144,28,64,0.7);
    }

    .swiper-button-prev .swiper-button-next {
      color:#fff;
    }

    :root{--swiper-theme-color:rgb(144,28,64)}

  </style>

</head>
<body>
  <?php include("includes/header.php"); ?>

  <main>
    <!-- Swiper -->
    <!-- <div class="swiper-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide" style="border:3px solid red;">
          <video id="videoplay" src="videos/vídeo_campanha_villa_santorini.mp4" type="video/mp4" autoplay loop muted width="100%"></video>
        </div>
        <div class="swiper-slide">
          <img src="img/9a6f86_ff80bc61a20540218cbedcacd10946dc.png" alt="">
        </div>
        <div class="swiper-slide">Slide 3</div>
        <div class="swiper-slide">Slide 4</div>
        <div class="swiper-slide">Slide 5</div>
        <div class="swiper-slide">Slide 6</div>
        <div class="swiper-slide">Slide 7</div>
        <div class="swiper-slide">Slide 8</div>
        <div class="swiper-slide">Slide 9</div>
        <div class="swiper-slide">Slide 10</div>
      </div>
      Add Pagination -->
      <!-- <div class="swiper-pagination"></div> -->
      <!-- Add Arrows -->
      <!-- <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div> -->
    <!-- </div> -->
    <div class="container-fluid" style="height:86vh; max-height:86vh">
      <div class="row">
        <div class="col-md-12" style="max-height:86vh;min-height:86vh;">
          <video id="videoplay" src="videos/vídeo_campanha_villa_santorini.mp4" type="video/mp4" autoplay loop muted style="width:100%;max-height:100%;height:100%"></video>
        </div>
      </div>
    </div>
    <div class="container-fluid" style="padding:140px 0px 130px 0px;background-image:url('img/86373.jpg')">
      <!-- BEM VINDO -->
      <div class="col-md-12 d-flex flex-row welcome" style="display: none !important;">
        <div class="col-md-6">
          <aside>
            <h2>More no Melhor da Região Oeste</h2>
            <img src="img/divider_line_big.svg">
          
            <p>
              Morar neste apartamento do bairro Palmeiras é estar certo de que você irá viver momentos inesquecíveis.
              Este imóvel lhe proporciona o ambiente perfeito para se divetir em família e receber amigos. São 3 quartos, 1 suíte e 2 vagas.
              Situado em um dos melhores pontos da região oeste de Belo Horizonte, o edifício possui uma estrutura que entrega a você uma
              experiência de bem-estar.
            </p>

            <div class="data-site d-flex flex-row align-items-center justify-content-center">
              <div class="col-md-12 d-flex flex-row">
                <div class="col-md-3 d-flex flex-column text-center">
                  <span class="number">67</span>
                  m²
                </div>
                <div class="col-md-3 d-flex flex-column text-center">
                  <span class="number">3</span>
                  Quartos
                </div>
                <div class="col-md-3 d-flex flex-column text-center">
                  <span class="number">1</span>
                  Suíte
                </div>
                <div class="col-md-3 d-flex flex-column text-center">
                  <span class="number">2</span>
                  Vagas
                </div>
              </div>
            </div>
          </aside>
        </div>
        <div class="col-md-6 d-flex flex-row justify-content-center">
          <img src="img/fachada.jpg" width="400" height="450">
        </div>
      </div>
    </div>
    
    <section class="p-0 pt-5 pb-5">
      <div class="container-fluid">
        <!-- SERVIÇOS -->
        <div class="col-md-12 pt-5 pb-5 text-center services" style="background: #f2f2f2;display: none !important;">
          <h2>Conheça nossos Produtos</h2>
          <img src="img/divider_line_big.svg" class="attachment-full size-full" alt="" data-attachment-id="47" data-permalink="https://themes.getmotopress.com/aquentro/home/divider_line_big/" data-orig-file="https://themes.getmotopress.com/aquentro/wp-content/uploads/sites/20/2018/07/divider_line_big.svg" data-orig-size="" data-comments-opened="1" data-image-meta="[]" data-image-title="divider_line_big" data-image-description="" data-medium-file="https://themes.getmotopress.com/aquentro/wp-content/uploads/sites/20/2018/07/divider_line_big.svg" data-large-file="https://themes.getmotopress.com/aquentro/wp-content/uploads/sites/20/2018/07/divider_line_big.svg">
          <div class="paragraph d-flex flex-row justify-content-center">
            <p>
              
            </p>
          </div>

          <div class="slider-secondary pt-1">
            <div class="col-md-12 d-flex flex-row">
              <div class="col-md-5 pl-5">
                <div class="col-md-6 slide-counter">
                  <span class="first-counter">01</span>
                  <span class="last-counter">/ 06</span>
                </div>
                <div class="slide-details">
                  <h2>Edifício Villaggio Palmeiras</h2>
                  <ul style="list-style-type:none;padding:0px 0px 0px 20px;margin:0px 0px 6px 0px;">
                    <li>- Torre única 100% revestida</li>
                    <li>- Elevador / água e gás individuais</li>
                    <li>- O melhor 2 quartos da região com suíte e 2 ou 3 vagas de garagem</li>
                    <li>- Infraestrutura completa na região: próximo a supermercados,
hospital, bancos, escolas, centros comerciais, etc
</li>
                    <li>- Lazer completo montado e mobiliado*: academia, espaço gourmet, salão
de festas, salão de jogos, playground</li>
                  </ul>
                  <a href="#" class="more-link" tabindex="0">Detalhes</a>
                  <i class="fas fa-caret-right"></i>
                </div>
              </div>
              <div class="col-md-7 d-flex flex-row justify-content-center">
                <!-- <img src="http://via.placeholder.com/709x430" alt="slide-photo">
                 -->
                <img src="img/9a6f86_bb03e751f4604d0a9579a45a547e19e6.png" alt="" width="709" height="430">
              </div>
            </div>
            </div>
        </div>
      </div>
    </section>

    <section class="p-0 pt-5">
      <div class="container-fluid">
        <!-- ALBÚM -->
        <div class="col-md-12 text-center rent" style="display: none !important;">
          <h2>Conheça nossos Empreendimentos</h2>
          <img src="img/divider_line_big.svg">
          <div class="paragraph d-flex flex-row justify-content-center">
            <p>
              <!-- Apartment amenities include a broadband internet access, satellite, hardwood floor, fireplace and more. You’ll be impressed with oversized windows and fantastic terraces. Services for short and long stays. -->
            </p>
          </div>
          <div class="col-md-12 d-flex flex-row">
            <div class="col-md-8">
              <!-- <img class="img-fluid" src="http://via.placeholder.com/1000x600" alt=""> -->
              <img src="img/9a6f86_ba2e98fcbb824b9e9320e7f25108d823.png" alt="" width="845" height="668">
            </div>
            <div class="col-md-4">
              <div class="col-md-12">
                <!-- <img class="img-fluid" src="http://via.placeholder.com/400x330" alt=""> -->
                <img src="img/varanda.png" alt="" width="400" height="330">
              </div>
              <div class="col-md-12 mt-2">
                <!-- <img class="img-fluid" src="http://via.placeholder.com/400x330" alt=""> -->
                <img src="img/predio.png" alt="" width="400" height="330">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="neigborhood p-0 pt-5 pb-5">
      <div class="container-fluid">
        <!-- LOCAIS -->
        <div class="col-md-12 pt-5 pb-5 text-center locale" style="display: none !important;background: #f2f2f2">
          <h2>Notícias Recentes</h2>
          <img src="img/divider_line_big.svg">

          <div class="col-md-12 d-flex flex-row pt-5">
            <div class="col-md-3 text-left pl-4">
              <div class="card-block">
                <div class="number">
                  01.
                  <span class="square"></span>
                </div>
                <h2 class="block-title">
                  <a class="square-link" href="#">Conheça a região Oeste</a>
                </h2>
                <img class="square-img" src="img/divider_line_big.svg">
                <p>
                  A região oeste  é uma região administrativa no município brasileiro de Belo Horizonte,
                  no estado de Minas Gerais, através desta região a Linha 1 do metrô, que a atravessa,
                  segue até às Estações Cidade Industrial e Eldorado na cidade vizinha...
                </p>
              </div>
            </div>
            <div class="col-md-3 text-left pl-4">
              <div class="card-block">
                <div class="number">
                  02.
                  <span class="square"></span>
                </div>
                <h2 class="block-title">
                  <a class="square-link" href="#">O que é ITBI ?</a>
                </h2>
                <img class="square-img" src="img/divider_line_big.svg">
                <p>
                  O Imposto sobre Transmissão de Bens Imóveis – ITBI – é um tributo
                  municipal a ser pago quando ocorre uma transferência imobiliária. Por ser
                  um imposto municipal, sua cobrança é realizada pela prefeitura da cidade
                  onde fica o imóvel adquirido. Em muitas...
                </p>
              </div>
            </div>
            <div class="col-md-3 text-left pl-4">
              <div class="card-block">
                <div class="number">
                  03.
                  <span class="square"></span>
                </div>
                <h2 class="block-title">
                  <a class="square-link" href="#">Pronto ou na planta ?</a>
                </h2>
                <img class="square-img" src="img/divider_line_big.svg">
                <p style="text-align:left;">
                ​  Todo mundo procura um lugar para chamar de seu e, finalmente, parar de pagar
                  aluguel. Mas, antes de começar esse projeto, é preciso escolher a forma de
                  pagamento e o tipo de imóvel mais adequado ao seu estilo de vida: na planta ou
                  pronto para morar.
                  Veja abaixo os principais cuidados...
                </p>
              </div>
            </div>
            <div class="col-md-3 text-left pl-4">
              <div class="card-block">
                <div class="number">
                  04.
                  <span class="square"></span>
                </div>
                <h2 class="block-title">
                  <a class="square-link" href="#">Queda do Selic para 4,5%</a>
                </h2>
                <img class="square-img" src="img/divider_line_big.svg">
                <p>
                  O Comitê de Política Monetária (Copom) do Banco Central (BC) decidiu, reduzir a taxa básica de juros (Selic) de 5% para 4,5% ao ano. O corte de meio ponto percentual veio em linha com a expectativa do mercado. Com a decisão, a Selic chegou ao menor percentual desde...
                </p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>

  </main>

  <?php
    include("includes/footer.php");
  ?>
 
  <!-- Whatsapp Call -->
  <div class="whatsapp">
    <a href="https://wa.me/5531999813151/?text=Olá%20estou%20no%20seu%20site%20e%20gostei%20dos%20imóveis,%20você%20pode%20me%20ajudar?" target="_blank"><img src="img/whatsapp.png" alt="" width="70px"></a>
  </div>

  <script src="js/jquery-3.4.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/all.min.js"></script>
  <script src="js/swiper.min.js"></script>

  <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
        type: 'progressbar',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>

  <script>

    // MENU
    $("nav ul li a").click(function(){
      $("nav ul li a").removeClass("link-active");
      $(this).addClass("link-active");
    })

    $(document).scroll(function(e){
      let posicao = $(document).scrollTop();
      if(posicao > 122){
        $(".welcome").fadeIn(1000);
        $(".navbar-site").addClass("nav-fixed");
      }else{
        $(".navbar-site").removeClass("nav-fixed");
      }

      if(posicao > 800){
        $(".services").fadeIn(1000)
      }

      if(posicao > 1600){
        $(".rent").fadeIn(1000);
      }

      if(posicao > 2500){
        $(".locale").fadeIn(1000);
      }
    })

  </script>
</body>
</html>