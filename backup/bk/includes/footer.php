<footer>
  <div class="footer-copy">
    <div class="footer-content d-flex flex-column align-items-center justify-content-center">
      <span class="address text-center">
        Rua dos Guajajaras - Centro, Belo Horizonte - MG, 30180-100 
      </span>
      <span class="tel text-center">
        (31) 3274-9001 (31) 99981-3151
      </span>
      <span class="social text-center">
        <ul class="text-center">
          <li><i class="fab fa-facebook"></i></li>
          <li><i class="fab fa-twitter"></i></li>
          <li><i class="fab fa-instagram"></i></li>
        </ul>
      </span>
      <span class="copyright text-center">
        <i class="far fa-copyright"></i> 2020 Todos os Direitos Reservados
      </span>
    </div>
  </div>
</footer>