<header>
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-3" style="padding-left:7%;">
          <img src="img/logo2.jpg" alt="Logomarca" class="logo" title="Mendes e Castro Construtora">
        </div>
        <div class="col-md-9">
          <nav class="d-flex flex-row justify-content-end align-items-end m-0">
            <ul class="m-0">
              <li data-active="active"><a href="/" class="link-active">Home</a></li>
              <li><a href="#">Lançamentos</a></li>
              <li><a href="#">Em Construção</a></li>
              <li><a href="#">Prontos para Morar</a></li>
              <li><a href="#">Quem Somos</a></li>
              <li><a href="#">Contato</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>