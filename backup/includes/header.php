<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Oswald|Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
    
    <link rel="stylesheet" href="css/count3_estilo.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Mendes e Castro Construtora</title>

    <style>
        /* font-family: 'Roboto', sans-serif;
    font-family: 'Oswald', sans-serif; */
        .swiper-container {
          width: 100%;
          height: 100%;
        }
    
        .swiper-slide {
          text-align: center;
          font-size: 18px;
          background: #fff;
    
          /* Center slide text vertically */
          display: -webkit-box;
          display: -ms-flexbox;
          display: -webkit-flex;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          -webkit-justify-content: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          -webkit-align-items: center;
          align-items: center;
        }
    
        .swiper-pagination-progressbar 
        .swiper-pagination-progressbar-fill {
          background: rgba(144,28,64,0.7);
        }
    
        .swiper-button-prev .swiper-button-next {
          color:#fff;
        }
    
        :root{--swiper-theme-color:rgb(144,28,64)}
    
      </style>
    
</head>
<body>
<header>
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-3" style="padding-left:7%;">
          <img src="img/7a1b9e8a-2f9b-48ab-859b-590c70c2b56b.jpg" alt="Logomarca" class="logo" title="Mendes e Castro Construtora">
        </div>
        <div class="col-md-9">
          <nav class="d-flex flex-row justify-content-end align-items-end m-0">
            <ul class="mt-0">
              <li data-active="active"><a href="/" class="link-active">Home</a></li>
              <li><a href="#">Lançamentos</a></li>
              <li><a href="#">Em Construção</a></li>
              <li><a href="#">Prontos para Morar</a></li>
              <li><a href="apartamentosavulsos.php">Apartamentos Avulsos</a></li>
              <li><a href="#">Quem Somos</a></li>
              <li><a href="#">Contato</a></li>
              
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>